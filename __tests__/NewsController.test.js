import { getNews, getNewsByTag } from "../src/controllers/NewsController.js";

const mockStoryRepository = {
  getNews: jest.fn(),
  getNewsByTag: jest.fn(),
};

test("should call StoryRepository.getNews with page param", async () => {
  const req = { params: { page: 2 } };
  const res = { json: jest.fn() };

  mockStoryRepository.getNews.mockResolvedValue([]);

  await getNews(req, res);

  expect(mockStoryRepository.getNews).toHaveBeenCalledWith(2);
});

test("should call StoryRepository.getNewsByTag with tag and page params", async () => {
  const req = { query: { page: 2 }, params: { tag: "story" } };
  const res = { json: jest.fn() };

  const mockResponse = [{ id: 1, title: "Test Story", tag: "technology" }];

  mockStoryRepository.getNewsByTag.mockResolvedValue(mockResponse);

  await getNewsByTag(req, res);

  expect(mockStoryRepository.getNewsByTag).toHaveBeenCalledWith("story", 2);
});
