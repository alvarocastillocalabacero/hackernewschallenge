import StoryRepository from '../src/repositories/StoryRepository.js'

test('getNews returns only stories with deletedAt null', async () => {
  const data = await StoryRepository.getNews(1);
  expect(data.results.every(story => story.deletedAt === null)).toBe(true);
});

test('getNewsByTag returns only stories with deletedAt null by tag', async () => {
  const data = await StoryRepository.getNewsByTag("story",1);
  expect(data.results.every(story => story.deletedAt === null)).toBe(true);
});

test('getNewsByTag returns only stories with deletedAt null by tag', async () => {
  const data = await StoryRepository.getNewsByTag("story",1);
  expect(data.results.every(story => story.deletedAt === null)).toBe(true);
});