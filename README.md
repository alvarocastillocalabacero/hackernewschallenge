# Hack New Challenge!
---
## Database Entity Relation Diagram
The following diagram was used to filter by titles and tags respectively

![Esta es una imagen de ejemplo](img/db.png)

## Configuration
```sh
npm run dev
```

Configure *.env* deleting *.example* extension from *.env* file

```sh
npm run dev
```

## Test
```sh
npx jest
```

#### For API
Use postman file , in delete request, use story_id from one of items 

---

### TO DO
* Use Docker
* Configure Pipelines
* More