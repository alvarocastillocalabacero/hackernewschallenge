import StoryRepository from "../repositories/StoryRepository.js";

/**
* Controller function to get news by page
* @function
* @async
* @param {object} req - Express request object
* @param {object} res - Express response object
* @returns {object} - Returns a JSON object with the news data
*/
export const getNews = async (req, res) => {
  try {
    const page = req.params.page || 1;
    const response = await StoryRepository.getNews(page);
    return res.json(response);
  } catch (err) {
    res.status(500).json({ Message: "Server Error" });
  }
};

/**
* Controller function to get news by tag and page
* @function
* @async
* @param {object} req - Express request object
* @param {object} res - Express response object
* @returns {object} - Returns a JSON object with the news data
*/
export const getNewsByTag = async (req, res) => {
  try {
    const page = req.query.page || 1; 
    const tag = req.params.tag;
    const response = await StoryRepository.getNewsByTag(tag, page);
    return res.json(response);
  } catch (err) {
    res.status(500).json({ Message: "Server Error" });
  }
};

/**
* Controller function to get news by title and page
* @function
* @async
* @param {object} req - Express request object
* @param {object} res - Express response object
* @returns {object} - Returns a JSON object with the news data
*/
export const getNewsByTitle = async (req, res) => {
  try {
    const page = req.query.page || 1; 
    const title = req.params.title;
    const response = await StoryRepository.getNewsByTitle(title, page);
    return res.json(response);
  } catch (err) {
    res.status(500).json({ Message: "Server Error" });
  }
};

/**
* Controller function to get a single news by id and page
* @function
* @async
* @param {object} req - Express request object
* @param {object} res - Express response object
* @returns {object} - Returns a JSON object with the news data
*/
export const getNew = async (req, res) => {
  try {
    if (req.params.id !== undefined) {
      const page = parseInt(req.query.page) || 1; 
      const id = parseInt(req.params.id);
      const response = await StoryRepository.getNew(id, page);
      return res.json(response);
    }
    res.json({ message: "id is null" });
  } catch (err) {
    res.status(500).json({ Message: "Server Error" });
  }
};

/**
* Controller function to delete a single news by id
* @function
* @async
* @param {object} req - Express request object
* @param {object} res - Express response object
* @returns {object} - Returns a message indicating whether the story was deleted or an error message
*/
export const deleteNew = async (req, res) => {
  try {
    if (req.params.id !== undefined) {
      const id = parseInt(req.params.id);
      const data = await StoryRepository.deleteNew(id);
      if (data) {
        return res.json("Story Deleted");
      } else {
        return res.status(500).json({ Message: "Server Error" });
      }
    }
    res.json({ message: "id is null" });
  } catch (err) {
    res.status(500).json({ Message: "Server Error" });
  }
};
