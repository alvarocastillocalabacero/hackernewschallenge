import Author from "../models/Author.js";
import Story from "../models/Story.js";
import Tag from "../models/Tag.js";
import StoryTag from "../models/StoryTag.js";
import { paginateResults } from "../utils/pagination.js";
import { Op } from "sequelize";

class StoryRepository {
  /**
   * Retrieves a paginated list of all news stories
   * @async
   * @function
   * @param {number} currentPage - The current page to retrieve from the paginated results
   * @returns {Promise<Object>} An object containing a list of stories and pagination information
   */
  async getNews(currentPage) {
    const limitNews = process.env.MAX_NEWS_PAGINATION || 5;
    const stories = await Story.findAll({
      where: { deletedAt: null },
      include: [
        {
          model: Tag,
          attributes: ["name"],
          through: { attributes: [] },
        },
        {
          model: Author,
          attributes: ["name"],
        },
      ],
    });
    return paginateResults(stories, limitNews, currentPage);
  }

  /**
   * Retrieves a paginated list of news stories by tag
   * @async
   * @function
   * @param {string} tag - The tag to filter by
   * @param {number} currentPage - The current page to retrieve from the paginated results
   * @returns {Promise<Object>} An object containing a list of stories and pagination information
   */
  async getNewsByTag(tag, currentPage) {
    const limitNews = process.env.MAX_NEWS_PAGINATION || 5;
    const stories = await Story.findAll({
      where: { deletedAt: null },
      include: [
        {
          model: Tag,
          where: { name: tag },
          attributes: ["name"],
          through: { attributes: [] },
        },
        {
          model: Author,
          attributes: ["name"],
        },
      ],
    });
    return paginateResults(stories, limitNews, currentPage);
  }

  /**
   * Retrieves a paginated list of news stories by title
   * @async
   * @function
   * @param {string} title - The title to search for
   * @param {number} currentPage - The current page to retrieve from the paginated results
   * @returns {Promise<Object>} An object containing a list of stories and pagination information
   */
  async getNewsByTitle(title, currentPage) {
    const limitNews = process.env.MAX_NEWS_PAGINATION || 5;

    const stories = await Story.findAll({
      include: [
        {
          model: Tag,
          attributes: ["name"],
          through: { attributes: [] },
        },
        {
          model: Author,
          attributes: ["name"],
        },
      ],
      where: {
        story_title: {
          deletedAt: null,
          [Op.like]: `%${title}%`,
        },
      },
    });

    return paginateResults(stories, limitNews, currentPage);
  }

  /**
   * Retrieves a paginated news story by id
   * @async
   * @function
   * @param {number} id - The id of the story to retrieve
   * @param {number} currentPage - The current page to retrieve from the paginated results
   * @returns {Promise<Object>} An object containing a list of stories and pagination information
   */
  async getNew(id, currentPage) {
    const limitNews = process.env.MAX_NEWS_PAGINATION || 5;
    const stories = await Story.findAll({
      include: [
        {
          model: Tag,
          attributes: ["name"],
          through: { attributes: [] },
        },
        {
          model: Author,
          attributes: ["name"],
        },
      ],
      where: {
        story_id: id,
        deletedAt: null,
      },
    });
    return paginateResults(stories, limitNews, currentPage);
  }

  /**
   *Soft-deletes a news story by id
   * @async
   * @function
   * @param {number} id - The id of the story to delete
   * @returns {Promise<number>} The number of rows affected (should be 1 if successful)
   */
  async deleteNew(id) {
    return await Story.update(
      { deletedAt: new Date() },
      { where: { story_id: id } }
    );
  }
}

export default new StoryRepository();
