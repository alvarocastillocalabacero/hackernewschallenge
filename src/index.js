import app from "./app.js";
import { sequelize } from "./database/database.js";
import Author from "./models/Author.js";
import Story from "./models/Story.js";
import Tag from "./models/Tag.js";
import StoryTag from "./models/StoryTag.js";

import { populateDB } from "./services/HackernewsService.js";

// Server Logic
async function main() {
  try {
    await sequelize.sync();
    populateDB();
    app.listen(app.get("port"));
    console.log("Server listening in port", app.get("port"));
    setInterval(populateDB, process.env.MAX_TIME_FETCH);
  } catch (error) {
    console.log("Server Error: ", error);
  }
}

main();
