import axios from "axios";
import Author from "../models/Author.js";
import Story from "../models/Story.js";
import Tag from "../models/Tag.js";
import StoryTag from "../models/StoryTag.js";
import { Op } from "sequelize";

/**
 * Fetches new stories from the API.
 * @throws {Error} Throws an error if the API does not return any news or if there's an error retrieving the data.
 * @returns {Array} An array containing the new stories.
 */
export async function fetchNewStories() {
  try {
    const response = await axios.get(process.env.URL_HACK_NEWS);
    const data = await response.data;
    if (!data.hits) {
      throw new Error("API sin noticias");
    }
    return data.hits;
  } catch (error) {
    throw new Error("Error al obtener los datos desde la api: ");
  }
}

/**
 * Determines if a story is valid based on its ID and deletedAt property.
 * @async
 * @function
 * @param {Object} options - Options object.
 * @param {number} options.story_id - ID of the story to validate.
 * @returns {Promise<boolean>} A boolean indicating if the story is valid or not.
 */
const isValidStory = async ({ story_id }) => {
  const story = await Story.findOne({
    where: {
      story_id: parseInt(story_id),
    },
  });

  if (story) {
    if (story.deletedAt !== null) {
      return false;
    }
  }
  return true;
};

/**
 * Fetches new stories and populates the database with them.
 * validate if news is deleted
 * @function
 * @returns {void}
 */
export const populateDB = () => {
  fetchNewStories().then((data) => {
    data.forEach(async (story) => {
      const storyData = {};
      if (story._tags[0] == "story") {
        storyData.story_id = parseInt(story.objectID);
        storyData.story_title = story._highlightResult.title.value;
        storyData.text = story._highlightResult.story_text
          ? story._highlightResult.story_text.value
          : "";
        storyData.object_id = parseInt(story.objectID);
      } else {
        storyData.story_id = story.story_id;
        storyData.story_title = story._highlightResult.story_title
          ? story._highlightResult.story_title.value
          : "";
        storyData.text = story.comment_text;
        storyData.object_id = story.objectID;
      }

      const isValid = await isValidStory(storyData);
      if (!isValid) {
        return;
      }

      const author = await Author.findOrCreate({
        where: { name: story._highlightResult.author.value },
      });
      storyData.AuthorId = author[0].id;

      const newStory = await Story.create(storyData);
      const tags = story._tags;
      tags.forEach(async (nameTag) => {
        const tag = await Tag.findOrCreate({
          where: { name: nameTag },
        });

        await StoryTag.create({
          StoryId: newStory.id,
          TagId: tag[0].id,
        });
      });
    });
  });
};
