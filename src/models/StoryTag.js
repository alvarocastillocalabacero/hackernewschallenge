import { DataTypes } from "sequelize";
import  {sequelize}  from "../database/database.js";
import Story from "./Story.js";
import Tag  from "./Tag.js";

const StoryTag = sequelize.define("StoryTag", {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
});

export default StoryTag;
