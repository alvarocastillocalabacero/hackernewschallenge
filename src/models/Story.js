import { DataTypes } from "sequelize";
import { sequelize } from "../database/database.js";
import Author from "./Author.js";
import Tag from "./Tag.js";
import StoryTag from "./StoryTag.js";

const Story = sequelize.define(
  "Story",
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    story_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    story_title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    text: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    object_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    deletedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: null,
    },
  },
  {
    paranoid: true, // Habilita el comportamiento de eliminación lógica
    deletedAt: "deletedAt", // Establece el nombre del campo que indica la fecha de eliminación lógica
    scopes: {
      active: {
        where: {
          deletedAt: null, // Filtra solo los registros que no han sido eliminados
        },
      },
    },
  }
);

Story.belongsTo(Author, { foreignKey: { allowNull: false } });
Author.hasMany(Story, { onDelete: "CASCADE" });

Story.belongsToMany(Tag, { through: StoryTag });
Tag.belongsToMany(Story, { through: StoryTag });

export default Story;
