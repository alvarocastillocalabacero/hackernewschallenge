import { DataTypes } from "sequelize";
import { sequelize } from "../database/database.js";
import Story from './Story.js'
import StoryTag from "./StoryTag.js";

const Tag = sequelize.define("Tag", {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
});


export default Tag;
