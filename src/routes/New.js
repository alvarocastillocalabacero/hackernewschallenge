import { Router } from "express";
import {
  deleteNew,
  getNews,
  getNew,
  getNewsByTag,
  getNewsByTitle,
} from "../controllers/NewsController.js";

const router = Router();

router.route("/").get(getNews);
router.route("/tag/:tag").get(getNewsByTag);
router.route("/title/:title").get(getNewsByTitle);
router.route("/:id").get(getNew).delete(deleteNew);

export default router;
