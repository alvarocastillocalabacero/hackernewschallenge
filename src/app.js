import express from "express";
import dotenv from "dotenv";
import cors from "cors";
import newsRouter from './routes/New.js'

const app = express();
dotenv.config()

// Configuration
app.set("port", process.env.PORT || 4000);

// Middlewares
app.use(cors());
app.use(express.json());

// Default route
app.get("/", (req, res) => {
  res.send("Hello World!");
});

// Api route
app.use('/api/hacknews', newsRouter)

app.use((req, res, next) => {
  res.status(404).send("Error 404 Not Found");
});

export default app;
